<?php

namespace App\Controller\Admin\Pessoa;

use App\Domain\Model\Ator;
use App\Infrastructure\DataTransferObject\AtorDto;
use App\Infrastructure\Enum\ResponseEnum;
use App\Infrastructure\Enum\TipoPessoaEnum;
use App\Infrastructure\Handler\AtorHandler;
use App\Infrastructure\Repository\AtorRepository;
use App\Presentation\Form\AtorType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * @Route(path="/pessoa")
 *
 * Class PessoaController
 * @package App\Controller\Admin\Pessoa
 */
class PessoaController extends AbstractController
{
    /**
     * @var AtorRepository
     */
    private $atorRepository;

    /**
     * @var Ator
     */
    private $empresaSession;

    public function __construct(
        AtorRepository $atorRepository,
        Security $security
    ) {
        $this->atorRepository = $atorRepository;
        $this->empresaSession = $security->getUser()->getAtor();
    }

    /**
     * @Route(path="/", name="listar-pessoas")
     *
     * @return Response
     */
    public function index()
    {
        $pessoas = $this->atorRepository
            ->findBy(
                [
                    'tipoAtor' => TipoPessoaEnum::CLIENTE_FORNECEDOR,
                    'empresa' => $this->empresaSession->getId()
                ]
            );

        return $this->render('@Admin/Pessoa/listar-pessoas.html.twig', [
            'pessoas' => $pessoas
        ]);
    }

    /**
     * @Route(path="/formulario/{id}", name="form-pessoa")
     *
     * @param int|null $id
     * @return RedirectResponse|Response
     */
    public function formulario(?int $id = null)
    {
        $url = '/pessoa/cadastrar';
        $pessoa = null;

        if($id != null) {
            $pessoa = $this->atorRepository
                ->findOneBy([
                    'id' => $id,
                    'empresa' => $this->empresaSession->getId(),
                    'tipoAtor' => TipoPessoaEnum::CLIENTE_FORNECEDOR
                ]);
            if (!$pessoa) {
                return $this->redirectToRoute('listar-pessoas', [
                    $this->addFlash('error','Cliente/Fornecedor não encontrado')
                ]);
            }
            $url = $pessoa ? '/pessoa/editar/'.$pessoa->getId() : $url;
        }

        $form = $this->createForm(AtorType::class, $pessoa);

        return $this->render('@Admin/Pessoa/form-pessoa.html.twig', [
            'url' => $url,
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route(path="/cadastrar", name="cadastrar-pessoa", methods={"POST"})
     *
     * @param Request $request
     * @return JsonResponse
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function cadastrar(Request $request)
    {
        try {
            $pessoa = new Ator();
            $form = $this->createForm(AtorType::class, $pessoa);
            $form->handleRequest($request);

            $atorDto = new AtorDto($pessoa);
            $atorDto->convertToPersist();
            $pessoa->setTipoAtor(TipoPessoaEnum::CLIENTE_FORNECEDOR);
            $pessoa->setEmpresa($this->empresaSession->getEmpresa()->getId());
            $this->getDoctrine()->getRepository(Ator::class)->salvar($pessoa);

            return new JsonResponse(
                [
                    ResponseEnum::JSON_CODE => Response::HTTP_OK,
                    ResponseEnum::JSON_MESSAGE => ResponseEnum::MENSAGEM_SUCESSO
                ], Response::HTTP_OK
            );
        }catch (Exception $e) {
            return new JsonResponse(
                [
                    ResponseEnum::JSON_CODE => Response::HTTP_BAD_REQUEST,
                    ResponseEnum::JSON_MESSAGE => ResponseEnum::MENSAGEM_ERRO
                ], Response::HTTP_BAD_REQUEST
            );
        }
    }

    /**
     * @Route(path="/editar/{id}", name="editar-pessoa", methods={"POST"})
     *
     * @param Request $request
     * @param int $id
     * @return JsonResponse
     */
    public function editar(Request $request, int $id)
    {
        try {
            $pessoa = $this->atorRepository
                ->findOneBy([
                    'id' => $id,
                    'empresa' => $this->empresaSession->getId(),
                    'tipoAtor' => TipoPessoaEnum::CLIENTE_FORNECEDOR
                ]);

            if (!$pessoa) {
                return $this->redirectToRoute('listar-pessoas', [
                    $this->addFlash('error','Cliente/Fornecedor não encontrado')
                ]);
            }
            $dadosForm = $this->createForm(AtorType::class, $pessoa);
            $dadosForm->handleRequest($request);
            $atorDto = new AtorDto($pessoa);
            $atorDto->convertToPersist();
            $this->atorRepository->salvar($pessoa);

            return new JsonResponse(
                [
                    ResponseEnum::JSON_CODE => Response::HTTP_OK,
                    ResponseEnum::JSON_MESSAGE => ResponseEnum::MENSAGEM_SUCESSO
                ], Response::HTTP_OK
            );
        } catch (\Exception $e) {
            return new JsonResponse(
                [
                    ResponseEnum::JSON_CODE => Response::HTTP_BAD_REQUEST,
                    ResponseEnum::JSON_MESSAGE => ResponseEnum::MENSAGEM_ERRO
                ],
                Response::HTTP_BAD_REQUEST
            );
        }
    }

    /**
     * @Route("/deletar/{id}", name="deletar-pessoa", methods={"DELETE"})
     */
    public function deletar($id = null)
    {
        $pessoa = $this->atorRepository->findOneBy(
                [
                    'id' => $id,
                    'empresa' => $this->empresaSession->getId(),
                    'tipoAtor' => TipoPessoaEnum::CLIENTE_FORNECEDOR
                ]
            );

        if (!$pessoa) {
            return new JsonResponse(['code' => 300, 'mensagem' => 'Erro ao deletar']);
        }

        try{
            $this->atorRepository->deletar($pessoa);
            return new JsonResponse(['code' => 200, 'mensagem' => 'Deletado com sucesso']);
        } catch (\Exception $e) {
            return new JsonResponse(['code' => '300', 'mensagem' => 'Erro ao deletar']);
        }
    }
}