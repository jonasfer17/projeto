<?php

namespace App\Controller\Admin\Configuracoes\Usuario;

use App\Domain\Model\PerfilUsuario;
use App\Presentation\Form\PerfilUsuarioType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class PerfilUsuarioController extends AbstractController
{
    /**
     * @Route(path="/usuario/perfil", name="listar-perfis-usuario")
     *
     * @return mixed
     */
    public function index()
    {
        $perfis = $this->getDoctrine()
            ->getRepository(PerfilUsuario::class)
            ->findAll();


        return $this->render('@Admin/Usuario/Perfil/listar-perfis.html.twig', [
            'perfis' => $perfis
        ]);
    }

    /**
     * @Route("/usuario/perfil/formulario/{id}", name="form-perfil-usuario")
     */
    public function formuarioPerfilUsuario(Request $request, $id = null)
    {
        $url = '/usuario/perfil/cadastrar';
        $perfil = null;

        if($id != null){
            $perfil = $this->getDoctrine()
                ->getRepository(PerfilUsuario::class)
                ->find($id);

            $url = !$perfil ? '/usuario/perfil/editar/'.$perfil->getId() : $url;
        }

        $form = $this->createForm(PerfilUsuarioType::class, $perfil);

        return $this->render('Admin/Usuario/Perfil/form-perfil-usuario.html.twig', [
            'url' => $url,
            'usuario' => $perfil,
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/usuario/perfil/cadastrar", name="cadastrar-perfil-usuario", methods={"POST"})
     */
    public function cadastrarPerfilUsuario(Request $request)
    {
        $perfil = new PerfilUsuario();
        $form = $this->createForm(PerfilUsuarioType::class, $perfil);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getRepository(PerfilUsuario::class)->salvar($perfil);

            return new JsonResponse(['code' => 200, 'mensagem' => 'Cadastrado com sucesso']);
        }

        return new JsonResponse(['code' => 300, 'mensagem' => 'Erro ao salvar']);
    }

    /**
     * @Route("/usuario/perfil/editar/{id}", name="editar-perfil-usuario", methods={"POST"})
     */
    public function editarPerfilUsuario(Request $request, int $id = null)
    {
        $perfil = $this->getDoctrine()
            ->getRepository(PerfilUsuario::class)
            ->find($id);

        if (!$perfil) {
            return new JsonResponse(['code' => 300, 'mensagem' => 'Erro ao salvar']);
        }

        $form = $this->createForm(PerfilUsuarioType::class, $perfil);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()
                ->getRepository(PerfilUsuario::class)
                ->salvar($perfil);

            return new JsonResponse(['code' => 200, 'mensagem' => 'Cadastrado com sucesso']);
        }
    }

    /**
     * @Route("/usuario/perfil/deletar/{id}", name="deletar-perfil-usuario", methods={"DELETE"})
     */
    public function deletarUsuarioAction(Request $request, $id = null)
    {
        $perfil = $this->getDoctrine()
            ->getRepository(PerfilUsuario::class)
            ->find($id);

        if (!$perfil) {
            return new JsonResponse(['code' => 300, 'mensagem' => 'Erro ao deletar']);
        }

        try{
            $this->getDoctrine()
                ->getRepository(PerfilUsuario::class)
                ->deletar($perfil);

            return new JsonResponse(['code' => 200, 'mensagem' => 'Deletado com sucesso']);
        } catch (\Exception $e) {

            return new JsonResponse(['code' => '300', 'mensagem' => 'Erro ao deletar']);
        }
    }
}