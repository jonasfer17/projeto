<?php

namespace App\Controller\Admin\Configuracoes\Usuario;

use App\Domain\Model\Usuario;
use App\Presentation\Form\UsuarioType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\User\User;

class UsuarioController extends AbstractController
{
    /**
     * @Route(path="/usuario", name="listar-usuarios")
     *
     * @return mixed
     */
    public function index()
    {
        $usuario = $this->getDoctrine()
            ->getRepository(Usuario::class)
            ->findAll();


        return $this->render('@Admin/Usuario/listar-usuarios.html.twig', [
            'usuarios' => $usuario
        ]);
    }

    /**
     * @Route("/usuario/formulario/{id}", name="form-usuario")
     */
    public function formularioUsuarioAction(Request $request, $id = null)
    {
        $url = '/usuario/cadastrar';
        $usuario = null;

        if($id != null){
            $usuario = $this->getDoctrine()
                ->getRepository(Usuario::class)
                ->find($id);

            $url = $usuario ? '/usuario/editar/'.$usuario->getId() : $url;
        }

        $form = $this->createForm(UsuarioType::class, $usuario);

        return $this->render('Admin/Usuario/form-usuario.html.twig', [
            'url' => $url,
            'usuario' => $usuario,
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/usuario/cadastrar", name="cadastrar-usuario", methods={"POST"})
     */
    public function cadastrarUsuario(Request $request, UserPasswordEncoderInterface $userPasswordEncoder)
    {
        $usuario = new Usuario();
        $form = $this->createForm(UsuarioType::class, $usuario);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $usuario->setPassword($userPasswordEncoder->encodePassword($usuario, $usuario->getPassword()));
            $usuario->setRoles($this->generateRoles());
            $this->getDoctrine()->getRepository(Usuario::class)->salvar($usuario);

            return new JsonResponse(['code' => 200, 'mensagem' => 'Cadastrado com sucesso']);
        }

        return new JsonResponse(['code' => 300, 'mensagem' => 'Erro ao salvar']);
    }

    /**
     * @Route("/usuario/editar/{id}", name="editar-usuario")
     */
    public function editarUsuarioAction(Request $request, $id = null)
    {
        $usuario = $this->getDoctrine()
            ->getRepository(Usuario::class)
            ->find($id);

        if (!$usuario) {
            return new JsonResponse(['code' => 300, 'mensagem' => 'Erro ao salvar']);
        }

        $form = $this->createForm(UsuarioType::class, $usuario);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()
                ->getRepository(Usuario::class)
                ->salvar($usuario);

            return new JsonResponse(['code' => 200, 'mensagem' => 'Editado com sucesso']);
        }
    }

    /**
     * @Route("usuario/deletar/{id}", name="deletar-usuario")
     */
    public function deletarUsuarioAction($id = null)
    {
        $usuario = $this->getDoctrine()
            ->getRepository(Usuario::class)
            ->find($id);

        if (!$usuario) {
            return new JsonResponse(['code' => 300, 'mensagem' => 'Erro ao deletar']);
        }

        try{
            $this->getDoctrine()
                ->getRepository(Usuario::class)
                ->deletar($usuario);
            
            return new JsonResponse(['code' => 200, 'mensagem' => 'Deletado com sucesso']);
        } catch (\Exception $e) {
            return new JsonResponse(['code' => '300', 'mensagem' => 'Erro ao deletar']);
        }
    }

    public function generateRoles()
    {
        return [
            json_encode(['0' => 'ROLE_USER', '1' => 'ROLE_ADMIN'], JSON_FORCE_OBJECT)
        ];
    }
}