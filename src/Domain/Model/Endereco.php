<?php

namespace App\Domain\Model;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Infrastructure\Repository\EnderecoRepository")
 * @ORM\Table(name="endereco")
 */
class Endereco
{
    /**
     * @var integer
     *
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var Ator
     *
     * @ORM\ManyToOne(targetEntity="Ator", inversedBy="endereco")
     * @ORM\JoinColumn(name="id_ator", referencedColumnName="id", nullable=false)
     */
    private $ator;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=200, nullable=true)
     */
    private $endereco;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private $numero;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private $cep;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=200, nullable=true)
     */
    private $bairro;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=200, nullable=true)
     */
    private $municipio;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=200, nullable=true)
     */
    private $complemento;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $codigoIbge;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private $estado;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return Ator
     */
    public function getAtor(): Ator
    {
        return $this->ator;
    }

    /**
     * @param Ator $ator
     */
    public function setAtor(Ator $ator): void
    {
        $this->ator = $ator;
    }

    /**
     * @return string
     */
    public function getEndereco(): ?string
    {
        return $this->endereco;
    }

    /**
     * @param string $rua
     */
    public function setEndereco(string $rua): void
    {
        $this->endereco = $rua;
    }

    /**
     * @return string
     */
    public function getNumero(): ?string
    {
        return $this->numero;
    }

    /**
     * @param string $numero
     */
    public function setNumero(string $numero): void
    {
        $this->numero = $numero;
    }

    /**
     * @return string
     */
    public function getCep(): ?string
    {
        return $this->cep;
    }

    /**
     * @param string $cep
     */
    public function setCep(string $cep): void
    {
        $this->cep = $cep;
    }

    /**
     * @return string
     */
    public function getBairro(): ?string
    {
        return $this->bairro;
    }

    /**
     * @param string $bairro
     */
    public function setBairro(string $bairro): void
    {
        $this->bairro = $bairro;
    }

    /**
     * @return string
     */
    public function getMunicipio(): ?string
    {
        return $this->municipio;
    }

    /**
     * @param string $municipio
     */
    public function setMunicipio(string $municipio): void
    {
        $this->municipio = $municipio;
    }

    /**
     * @return string
     */
    public function getComplemento(): ?string
    {
        return $this->complemento;
    }

    /**
     * @param string $complemento
     */
    public function setComplemento(string $complemento): void
    {
        $this->complemento = $complemento;
    }

    /**
     * @return int|null
     */
    public function getCodigoIbge(): ?int
    {
        return $this->codigoIbge;
    }

    /**
     * @param int $codigoIbge
     */
    public function setCodigoIbge(int $codigoIbge): void
    {
        $this->codigoIbge = $codigoIbge;
    }

    /**
     * @return string
     */
    public function getEstado(): ?string
    {
        return $this->estado;
    }

    /**
     * @param string $estado
     */
    public function setEstado(string $estado): void
    {
        $this->estado = $estado;
    }
}