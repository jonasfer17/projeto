<?php

namespace App\Domain\Model;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints\Date;

/**
 * @ORM\Entity(repositoryClass="App\Infrastructure\Repository\AtorRepository")
 * @ORM\Table(name="ator")
 */
class Ator
{
    /**
     * @var integer
     *
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_empresa", type="integer", nullable=false)
     */
    private $empresa;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer", nullable=false)
     */
    private $tipoAtor;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=200, nullable=true)
     */
    private $nome;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=200, nullable=true)
     */
    private $fantasia;

    /**
     * @var integer
     *
     * @ORM\Column(name="pf_pj", type="integer", nullable=false)
     */
    private $tipoPessoa;

    /**
     * @var string
     *
     * @ORM\Column(name="cpf_cnpj", type="string", length=20, nullable=true)
     */
    private $cpfCnpj;

    /**
     * @var string
     *
     * @ORM\Column(name="rg_ie", type="string", length=20, nullable=true)
     */
    private $rgIe;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=1, nullable=true)
     */
    private $sexo;

    /**
     * @var string
     *
     * @ORM\Column(type="date", nullable=true)
     */
    private $dataNascimento;

    /**
     * @var Endereco
     *
     * @ORM\OneToOne(targetEntity="Endereco", mappedBy="ator", cascade={"persist", "remove"})
     */
    private $endereco;

    /**
     * @var Contato
     *
     * @ORM\OneToOne(targetEntity="Contato", mappedBy="ator", cascade={"persist", "remove"})
     */
    private $contato;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return int|null
     */
    public function getEmpresa(): ?int
    {
        return $this->empresa;
    }

    /**
     * @param int $empresa
     */
    public function setEmpresa(int $empresa): void
    {
        $this->empresa = $empresa;
    }

    /**
     * @return int|null
     */
    public function getTipoAtor(): ?int
    {
        return $this->tipoAtor;
    }

    /**
     * @param int $tipoAtor
     */
    public function setTipoAtor(int $tipoAtor): void
    {
        $this->tipoAtor = $tipoAtor;
    }

    /**
     * @return null|string
     */
    public function getNome(): ?string
    {
        return $this->nome;
    }

    /**
     * @param string $nome
     */
    public function setNome(string $nome): void
    {
        $this->nome = $nome;
    }

    /**
     * @return null|string
     */
    public function getFantasia(): ?string
    {
        return $this->fantasia;
    }

    /**
     * @param string $fantasia
     */
    public function setFantasia(string $fantasia): void
    {
        $this->fantasia = $fantasia;
    }

    /**
     * @return int|null
     */
    public function getTipoPessoa(): ?int
    {
        return $this->tipoPessoa;
    }

    /**
     * @param int $tipo
     */
    public function setTipoPessoa(int $tipo): void
    {
        $this->tipoPessoa = $tipo;
    }

    /**
     * @return null|string
     */
    public function getCpfCnpj(): ?string
    {
        return $this->cpfCnpj;
    }

    /**
     * @param null|string $cpfCnpj
     */
    public function setCpfCnpj(?string $cpfCnpj): void
    {
        $this->cpfCnpj = $cpfCnpj;
    }

    /**
     * @return null|string
     */
    public function getRgIe(): ?string
    {
        return $this->rgIe;
    }

    /**
     * @param null|string $rgIe
     */
    public function setRgIe(?string $rgIe): void
    {
        $this->rgIe = $rgIe;
    }

    /**
     * @return null|string
     */
    public function getSexo(): ?string
    {
        return $this->sexo;
    }

    /**
     * @param string $sexo
     */
    public function setSexo(string $sexo): void
    {
        $this->sexo = $sexo;
    }

    /**
     * @return \DateTime|null
     */
    public function getDataNascimento(): ?\DateTime
    {
        return $this->dataNascimento;
    }

    /**
     * @param $dataNascimento
     */
    public function setDataNascimento($dataNascimento): void
    {
        $this->dataNascimento = $dataNascimento;
    }

    /**
     * @return Endereco|null
     */
    public function getEndereco(): ?Endereco
    {
        return $this->endereco;
    }

    /**
     * @param Endereco $endereco
     */
    public function setEndereco(Endereco $endereco): void
    {
        $this->endereco = $endereco;
    }

    /**
     * @return Contato|null
     */
    public function getContato(): ?Contato
    {
        return $this->contato;
    }

    /**
     * @param Contato $contato
     */
    public function setContato($contato): void
    {
        $this->contato = $contato;
    }
}