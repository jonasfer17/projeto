<?php

namespace App\Domain\Model;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass="App\Infrastructure\Repository\UsuarioRepository")
 * @ORM\Table(name="usuario")
 */
class Usuario implements UserInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var Ator
     *
     * @ORM\OneToOne(targetEntity="Ator")
     * @ORM\JoinColumn(name="id_ator", referencedColumnName="id", nullable=false)
     *
     */
    private $ator;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     */
    private $login;

    /**
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     */
    private $senha;

    /**
     * @var PerfilUsuario
     *
     * @ORM\ManyToOne(targetEntity="PerfilUsuario")
     * @ORM\JoinColumn(name="id_perfil", referencedColumnName="id", nullable=false)
     */
    private $perfil;

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Ator
     */
    public function getAtor(): Ator
    {
        return $this->ator;
    }

    /**
     * @param Ator $ator
     */
    public function setAtor(Ator $ator): void
    {
        $this->ator = $ator;
    }


    public function getLogin(): ?string
    {
        return $this->login;
    }

    public function setLogin(string $login): self
    {
        $this->login = $login;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->login;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;

        return array_unique($roles[0]);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword()
    {
        return $this->senha;
    }

    public function setPassword(string $password)
    {
        $this->senha = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    /**
     * @return PerfilUsuario|null
     */
    public function getPerfil(): ?PerfilUsuario
    {
        return $this->perfil;
    }

    /**
     * @param PerfilUsuario $perfil
     */
    public function setPerfil(PerfilUsuario $perfil): void
    {
        $this->perfil = $perfil;
    }

}
