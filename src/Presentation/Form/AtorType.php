<?php

namespace App\Presentation\Form;

use App\Domain\Model\Ator;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AtorType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nome', TextType::class,
                [
                    'label' => 'Nome',
                    'attr' => ['class' => 'form-control']
                ]
            )
            ->add('tipoPessoa', ChoiceType::class,
                [
                    'label' => 'Tipo de pessoa',
                    'choices' => [
                        'Pessoa Física' => 1,
                        'Pessoa Jurídica' => 2
                    ],
                    'attr' => [
                        'class' => 'form-control',
                    ]
                ]
            )
            ->add('fantasia', TextType::class,
                [
                    'label' => 'Nome Fantasia',
                    'attr' => ['class' => 'form-control'],
                    'required' => false
                ]
            )
            ->add('cpfCnpj', TextType::class,
                [
                    'label' => 'CPF',
                    'label_attr' => [
                        'id' => 'label_cpf_cnpj'
                    ],
                    'attr' => ['class' => 'form-control form-mutted'],
                    'required' => false
                ]
            )
            ->add('rgIe', TextType::class,
                [
                    'label' => 'RG',
                    'label_attr' => [
                        'id' => 'label_rg_ie'
                    ],
                    'attr' => ['class' => 'form-control form-mutted number'],
                    'required' => false
                ]
            )
            ->add('sexo', ChoiceType::class,
                [
                    'label' => 'Sexo',
                    'choices' => [
                        'Selecione' => null,
                        'Feminino' => 'F',
                        'Masculino' => 'M'
                    ],
                    'attr' => ['class' => 'form-control'],
                    'required' => false
                ]
            )
            ->add('dataNascimento', DateType::class,
                [
                    'label' => 'Data de Nascimento',
                    'attr' => ['class' => 'form-control date'],
                    'required' => false,
                    'empty_data' => null,
                    'widget' => 'single_text',
                    'format' => 'dd/MM/yyyy'
                ]
            )
            ->add('contato', ContatoType::class,
                [
                    'label' => ''
                ]
            )
            ->add('endereco', EnderecoType::class,
                [
                    'label' => ''
                ]
            )
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Ator::class,
        ]);
    }
}