<?php

namespace App\Presentation\Form;

use App\Domain\Model\Contato;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ContatoType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('telefone', TextType::class,
                [
                    'label' => 'Telefone',
                    'attr' => ['class' => 'form-control phone'],
                    'required' => false
                ]
            )
            ->add('celular', TextType::class,
                [
                    'label' => 'Celular',
                    'attr' => ['class' => 'form-control phone'],
                    'required' => false
                ]
            )
            ->add('email', EmailType::class,
                [
                    'label' => 'Email',
                    'attr' => ['class' => 'form-control'],
                    'required' => false
                ]
            )
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Contato::class,
        ]);
    }
}