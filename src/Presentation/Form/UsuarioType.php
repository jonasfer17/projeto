<?php

namespace App\Presentation\Form;

use App\Domain\Model\PerfilUsuario;
use App\Domain\Model\Usuario;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class UsuarioType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('login', TextType::class, [
                'label' => 'Login',
                'attr' => ['class' => 'form-control']
                ] 
            )
            ->add('password', PasswordType::class, [
                'label' => 'Senha',
                'attr' => [
                    'class' => 'form-control',
                    'autocomplete' => 'false'
                ]
            ])
            ->add('perfil', EntityType::class, [
                'class' => PerfilUsuario::class,
                'choice_label' => 'nome',
                'attr' => [
                    'class' => 'form-control'
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Usuario::class,
        ]);
    }
}
