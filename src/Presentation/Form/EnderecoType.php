<?php

namespace App\Presentation\Form;

use App\Domain\Model\Endereco;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EnderecoType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('endereco', TextType::class,
                [
                    'label' => 'Endereço',
                    'attr' => ['class' => 'form-control'],
                    'required' => false
                ]
            )
            ->add('numero', TextType::class,
                [
                    'label' => 'Número',
                    'attr' => ['class' => 'form-control'],
                    'required' => false
                ]
            )
            ->add('cep', TextType::class,
                [
                    'label' => 'CEP',
                    'attr' => ['class' => 'form-control cep'],
                    'required' => false
                ]
            )
            ->add('bairro', TextType::class,
                [
                    'label' => 'Bairro',
                    'attr' => ['class' => 'form-control'],
                    'required' => false
                ]
            )
            ->add('municipio', TextType::class,
                [
                    'label' => 'Municipio',
                    'attr' => ['class' => 'form-control'],
                    'required' => false
                ]
            )
            ->add('estado', ChoiceType::class,
                [
                    'label' => 'Estado',
                    'choices' => [
                        'Selecione' => null,
                        'Paraiba' => 'PB'
                    ],
                    'choice_value' => function ($value) {
                        return $value;
                    } ,
                    'attr' => ['class' => 'form-control'],
                    'required' => false
                ]
            )
            ->add('codigoIbge', TextType::class,
                [
                    'label' => '',
                    'attr' => ['class' => 'form-control'],
                    'required' => false
                ]
            )
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Endereco::class,
        ]);
    }
}