<?php

namespace App\Infrastructure\Repository;

use App\Domain\Model\PerfilUsuario;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

class PerfilUsuarioRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, PerfilUsuario::class);
    }

    /**
     * @param PerfilUsuario $perfilUsuario
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function salvar(PerfilUsuario $perfilUsuario)
    {
        $this->getEntityManager()->persist($perfilUsuario);
        $this->getEntityManager()->flush();
    }

    /**
     * @param PerfilUsuario $perfilUsuario
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function deletar(PerfilUsuario $perfilUsuario)
    {
        $this->getEntityManager()->remove($perfilUsuario);
        $this->getEntityManager()->flush();
    }
}