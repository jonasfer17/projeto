<?php

namespace App\Infrastructure\Repository;

use App\Domain\Model\Ator;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

class AtorRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Ator::class);
    }

    /**
     * @param Ator $ator
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function salvar(Ator $ator)
    {
        $this->getEntityManager()->persist($ator);
        $this->getEntityManager()->flush();
    }

    /**
     * @param Ator $ator
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function deletar(Ator $ator)
    {
        $this->getEntityManager()->remove($ator);
        $this->getEntityManager()->flush();
    }
}