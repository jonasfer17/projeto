<?php

namespace App\Infrastructure\Common;


class ConvertToDate
{
    /**
     * @param null|string $data
     * @return null|string|string[]
     */
    public static function converterDateToPersist(?string $data)
    {
        return $data
            ? new \DateTime(date('Y-m-d', strtotime($data)))
            : null;
    }

    /**
     * @param null|string $data
     * @return null|string|string[]
     */
    public static function converterDateToView(?\DateTime $data)
    {
        return $data
            ? $data->format('d/m/Y')
            : null;
    }
}