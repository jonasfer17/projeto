<?php

namespace App\Infrastructure\Common;


class SomenteNumeros
{
    /**
     * @param null|string $valor
     * @return null|string|string[]
     */
    public static function converter(?string $valor)
    {
        return $valor
            ? preg_replace("/[^0-9]/", "", $valor)
            : null;
    }
}