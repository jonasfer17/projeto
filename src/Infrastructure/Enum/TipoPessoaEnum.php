<?php

namespace App\Infrastructure\Enum;


class TipoPessoaEnum
{
    const EMPRESA = 1;
    const CLIENTE_FORNECEDOR = 2;
}