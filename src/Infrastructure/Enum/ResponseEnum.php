<?php

namespace App\Infrastructure\Enum;

class ResponseEnum
{
    const JSON_CODE = 'code';
    const JSON_MESSAGE = 'mensagem';
    const MENSAGEM_SUCESSO = 'Dados salvos';
    const MENSAGEM_ERRO = 'Erro ao salvar';
}