<?php

namespace App\Infrastructure\DataTransferObject;


use App\Domain\Model\Ator;
use App\Infrastructure\Common\ConvertToDate;
use App\Infrastructure\Common\SomenteNumeros;

class AtorDto
{
   /** @var Ator  */
    protected $ator;

    public function __construct(Ator $ator)
    {
        $this->ator = $ator;
    }


    public function convertToPersist()
    {
        $enderecoDto = new EnderecoDto($this->ator->getEndereco());
        $contatoDto = new ContatoDto($this->ator->getContato());
        $this->ator->setCpfCnpj(SomenteNumeros::converter($this->ator->getCpfCnpj()));
        $this->ator->setRgIe(SomenteNumeros::converter($this->ator->getRgIe()));
        $this->ator->setDataNascimento($this->ator->getDataNascimento());
        $this->ator->setEndereco($enderecoDto->convertToPersist());
        $this->ator->setContato($contatoDto->convertToPersist());
        $this->ator->getEndereco()->setAtor($this->ator);
        $this->ator->getContato()->setAtor($this->ator);
    }
}