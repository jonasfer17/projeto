<?php

namespace App\Infrastructure\DataTransferObject;

use App\Domain\Model\Endereco;
use App\Infrastructure\Common\SomenteNumeros;

class EnderecoDto
{
    /** @var Endereco  */
    private $endereco;

    public function __construct(Endereco $endereco)
    {
        $this->endereco = $endereco;
    }

    /**
     * @return Endereco
     */
    public function convertToPersist()
    {
        $this->endereco->setCodigoIbge(SomenteNumeros::converter($this->endereco->getCodigoIbge()));
        $this->endereco->setCep(SomenteNumeros::converter($this->endereco->getCep()));

        return $this->endereco;
    }
}