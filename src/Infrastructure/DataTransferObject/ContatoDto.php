<?php

namespace App\Infrastructure\DataTransferObject;

use App\Domain\Model\Contato;
use App\Infrastructure\Common\SomenteNumeros;

class ContatoDto
{
    /** @var Contato  */
    protected $contato;

    public function __construct(Contato $contato)
    {
        $this->contato = $contato;
    }

    /**
     * @return Contato
     */
    public function convertToPersist()
    {
        $this->contato->setTelefone(SomenteNumeros::converter($this->contato->getTelefone()));
        $this->contato->setCelular(SomenteNumeros::converter($this->contato->getCelular()));

        return $this->contato;
    }
}