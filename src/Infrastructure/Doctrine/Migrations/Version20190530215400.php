<?php

declare(strict_types=1);

namespace App\Infrastructure\Doctrine\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190530215400 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE pessoa_contato (id INT AUTO_INCREMENT NOT NULL, id_pessoa INT NOT NULL, telefone VARCHAR(20) DEFAULT NULL, celular VARCHAR(20) DEFAULT NULL, email VARCHAR(200) DEFAULT NULL, INDEX IDX_A5069B753AE81E6F (id_pessoa), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE pessoa_endereco (id INT AUTO_INCREMENT NOT NULL, id_pessoa INT NOT NULL, endereco VARCHAR(200) DEFAULT NULL, numero VARCHAR(20) DEFAULT NULL, cep VARCHAR(20) DEFAULT NULL, bairro VARCHAR(200) DEFAULT NULL, municipio VARCHAR(200) DEFAULT NULL, complemento VARCHAR(200) DEFAULT NULL, codigo_ibge INT DEFAULT NULL, estado VARCHAR(20) DEFAULT NULL, INDEX IDX_403BF1313AE81E6F (id_pessoa), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE usuario_perfil (id INT AUTO_INCREMENT NOT NULL, nome VARCHAR(180) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE pessoa (id INT AUTO_INCREMENT NOT NULL, tipo INT NOT NULL, nome VARCHAR(200) DEFAULT NULL, fantasia VARCHAR(200) DEFAULT NULL, cpf_cnpj VARCHAR(20) DEFAULT NULL, rg_ie VARCHAR(20) DEFAULT NULL, sexo VARCHAR(1) DEFAULT NULL, data_nascimento DATE DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE usuario (id INT AUTO_INCREMENT NOT NULL, id_perfil INT NOT NULL, login VARCHAR(180) NOT NULL, roles JSON NOT NULL, senha VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_2265B05DAA08CB10 (login), INDEX IDX_2265B05DB052C3AA (id_perfil), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE pessoa_contato ADD CONSTRAINT FK_A5069B753AE81E6F FOREIGN KEY (id_pessoa) REFERENCES pessoa (id)');
        $this->addSql('ALTER TABLE pessoa_endereco ADD CONSTRAINT FK_403BF1313AE81E6F FOREIGN KEY (id_pessoa) REFERENCES pessoa (id)');
        $this->addSql('ALTER TABLE usuario ADD CONSTRAINT FK_2265B05DB052C3AA FOREIGN KEY (id_perfil) REFERENCES usuario_perfil (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE usuario DROP FOREIGN KEY FK_2265B05DB052C3AA');
        $this->addSql('ALTER TABLE pessoa_contato DROP FOREIGN KEY FK_A5069B753AE81E6F');
        $this->addSql('ALTER TABLE pessoa_endereco DROP FOREIGN KEY FK_403BF1313AE81E6F');
        $this->addSql('DROP TABLE pessoa_contato');
        $this->addSql('DROP TABLE pessoa_endereco');
        $this->addSql('DROP TABLE usuario_perfil');
        $this->addSql('DROP TABLE pessoa');
        $this->addSql('DROP TABLE usuario');
    }
}
