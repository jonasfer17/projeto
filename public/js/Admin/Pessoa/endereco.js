$(document).ready(function() {

    $('#ator_endereco_codigoIbge').hide();

    function limpa_formulário_cep() {
        // Limpa valores do formulário de cep.
        $("#ator_endereco_endereco").val("");
        $("#ator_endereco_bairro").val("");
        $("#ator_endereco_municipio").val("");
        $("#ator_endereco_estado").val("");
        $("#ator_endereco_codigoIbge").val("");
    }

    //Quando o campo cep perde o foco.
    $("#ator_endereco_cep").blur(function() {

        //Nova variável "cep" somente com dígitos.
        var cep = $(this).val().replace(/\D/g, '');

        //Verifica se campo cep possui valor informado.
        if (cep != "") {

            //Expressão regular para validar o CEP.
            var validacep = /^[0-9]{8}$/;

            //Valida o formato do CEP.
            if(validacep.test(cep)) {

                //Preenche os campos com "..." enquanto consulta webservice.
                $("#ator_endereco_endereco").val("...");
                $("#ator_endereco_bairro").val("...");
                $("#ator_endereco_municipio").val("...");
                $("#ator_endereco_estado").val("...");
                $("#ator_endereco_codigoIbge").val("...");

                //Consulta o webservice viacep.com.br/
                $.getJSON("https://viacep.com.br/ws/"+ cep +"/json/?callback=?", function(dados) {

                    if (!("erro" in dados)) {
                        //Atualiza os campos com os valores da consulta.
                        $("#ator_endereco_endereco").val(dados.logradouro);
                        $("#ator_endereco_bairro").val(dados.bairro);
                        $("#ator_endereco_municipio").val(dados.localidade);
                        $("#ator_endereco_estado").val(dados.uf);
                        $("#ator_endereco_codigoIbge").val(dados.ibge);
                    } //end if.
                    else {
                        //CEP pesquisado não foi encontrado.
                        limpa_formulário_cep();
                        alert("CEP não encontrado.");
                    }
                });
            } //end if.
            else {
                //cep é inválido.
                limpa_formulário_cep();
                alert("Formato de CEP inválido.");
            }
        } //end if.
        else {
            //cep sem valor, limpa formulário.
            limpa_formulário_cep();
        }
    });
});
