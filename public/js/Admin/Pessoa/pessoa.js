$(document).ready(function(){
    if ($('#ator_tipo').val('1')) {
        $('.juridica').hide();
        $('#ator_cpfCnpj').mask('000.000.000-00');
    }

    $('#ator_tipo').on('change', function() {
        if ($('#ator_tipo').val() == 1) {
            $('.juridica').hide();
            $('.fisica').show();
            $('#ator_cpfCnpj').mask('000.000.000-00');
            $('#label_cpf_cnpj').html('CPF');
            $('#label_rg_ie').html('RG');
            $('.form-mutted').val('');
        } else {
            $('.fisica').hide();
            $('.juridica').show();
            $('#label_cpf_cnpj').html('CNPJ');
            $('#ator_cpfCnpj').mask('00.000/0000.00')
            $('#label_rg_ie').html('Inscrição Estadual')
            $('.form-mutted').val('');
        }
    })
})