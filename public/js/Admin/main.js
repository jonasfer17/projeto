function alert_sucesso(mensagem, url_redirect = null) {
    swal({
        title: mensagem,
        type: 'success',
        confirmButtonColor: '#3085d6',
        confirmButtonText: 'OK'
    }).then((result) => {
        window.location.href = URL + url_redirect;
    })
}

function alert_erro(mensagem, url_redirect = null) {
    swal({
        title: mensagem,
        type: 'error',
        confirmButtonColor: '#3085d6',
        confirmButtonText: 'OK'
    }).then((result) => {
        window.location.href = URL + url_redirect;
    })
}

function alert_delete(elemento, titulo, mensagem, url_delete) {
    swal({
        title: titulo,
        text: mensagem,
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Sim',
        cancelButtonText: 'Não',
        showLoaderOnConfirm: true,
        preConfirm: function() {
            return new Promise(function(resolve) {
                $.ajax({
                    url: URL+url_delete,
                    type: 'DELETE',
                    dataType: 'json'
                })
                    .done(function(response){
                        console.log(response);
                        if (response.code == 200) {
                            swal(response.mensagem, '' ,'success');
                            let tr = $(elemento).closest('tr');
                            tr.fadeOut(400, function(){
                                tr.remove();
                            });
                        } else {
                            swal('Oops...', 'Estamos com alguns problemas... tente novamente', 'error');
                        }
                    })
                    .fail(function(){
                        swal('Oops...', 'Estamos com alguns problemas... tente novamente', 'error');
                    });
            });
        },
        allowOutsideClick: false
    });
}


function bloqueiaBotao(button) {
    button.attr("disabled", true)
}

function desbloquearBotao(button) {
    button.attr("disabled", false)
}

function montarUrl(url) {
    url = url.split('/');
    if (Number.isInteger(parseInt(url[url.length -1]))) {
        url.pop();
    }
    url.pop();
    return url.join('/');
}

let botaoSalvar = $('#btn-salvar');

$("#form").on('submit', function(e) {
    bloqueiaBotao(botaoSalvar);
    e.preventDefault();
    var uri = $(this).attr('data-url');
    var txt = $(this).serialize();
    $.ajax({
        url: URL+uri,
        type: 'POST',
        data: txt,
        datatype: 'json',
        success:function(data){
            if(data.code === 200){
                alert_sucesso(data.mensagem, montarUrl(uri));
            }else{
                desbloquearBotao(botaoSalvar);
            }
        },
        error:function(data){
            desbloquearBotao(botaoSalvar);
        }
    });
})

$(".btn-delete").on('click', function() {
    let url = $(this).attr('data-url');
    let id = $(this).data('id');
    alert_delete(this,'Deletar', 'Tem certeza que deseja deletar?', url)
})


// Mascaras

var behavior = function (val) {
        return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
    },
    options = {
        onKeyPress: function (val, e, field, options) {
            field.mask(behavior.apply({}, arguments), options);
        }
    };

$('.phone').mask(behavior, options);
$('.date').mask('00/00/0000');
$('.cep').mask('00000-000');
$('.cpf').mask('000.000.000-00', {reverse: true});
$('.money').mask('000.000.000.000.000,00', {reverse: true});
$('.number').mask('00000000000');